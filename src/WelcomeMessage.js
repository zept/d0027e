import React from 'react';
import logo from './img/BB_header.png';

class WelcomeMessage extends React.Component {

  render() {
    return (
      <div class="row justify-content-md-center">
        <div class="col-xs-6 my-auto">
          <img src={logo} alt="Logo" />
          <br/>
          <br/>
          <p>Här presenteras information specifik för kontots behörighet. Detta kan involvera instruktioner, driftmeddelanden eller andra notifikationer. </p>
        </div>
      </div>
      );
  }
}

export default WelcomeMessage;
