import React from 'react';
import Gallery from 'react-grid-gallery';
import Alert from 'react-bootstrap/Alert';
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import ButtonToolbar from 'react-bootstrap/ButtonToolbar';
import Button from 'react-bootstrap/Button';
import Overlay from 'react-bootstrap/Overlay';
import Popover from 'react-bootstrap/Popover';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';


class PictureGallery extends React.Component {
  constructor(props) {
    super(props);
    {/* Alla sökparametrar här */}
    this.state = {images: props.imageGallery,
                  tags: [],
                  currentImage: 0};

    this.onCurrentImageChange = this.onCurrentImageChange.bind(this);
    this.handleBuyImage = this.handleBuyImage.bind(this);
  }

  componentDidMount() {
    // Save all tags from all pictures
    let tagList = [];

    this.props.imageGallery.map(image => {
      image.tags.map(tag => {
        if (tagList.indexOf(tag.value) === -1) {
          tagList.push(tag.value);
        }
      })
    })
    let tagObject = []

    tagList.map(tag => {
      tagObject.push({title: tag,
                    value: true});
    })

    this.setState({tags: tagObject});
  }

  handleFilters = (e) => {
    let filters = this.state.tags.slice();
    filters.map(tag => {
      if (tag.title === e.target.id) {
        tag.value = !tag.value;
      }
    });
    this.setState({
      tags: filters
    },this.filterImages())
  };

  showNumberofPictures() {
      return (
        <Alert key='0' variant={this.state.images.length > 0 ? 'success' : 'warning'}>
          {this.state.images.length} bilder hittades med valda söktermer och filter.
        </Alert>
        );
    }

  renderFilters() {
    return (
      <Form>
      {this.state.tags.map(tag => (
        <div key={tag.title} className="mb-0">
          <Form.Check
            type='checkbox'
            id={tag.title}
            label={tag.title}
            checked={tag.value}
            onChange={this.handleFilters}
          />
        </div>
      ))}
    </Form>);
  }

  filterImages() {
    console.log("Filtering");
    let imageList = [];

    this.props.imageGallery.map(image => {
      image.tags.map(tag => {
        this.state.tags.map(stateTag => {
          if (stateTag.title === tag.title & stateTag.value & imageList.indexOf(image) === -1) {
            imageList.push(image);
          }
        })
      })
    })
    this.setState({images: imageList});
  }

  onCurrentImageChange(index) {
    this.setState({ currentImage: index });
  }

  displayMetadata() {
    if (this.state.images.length > 0 & this.state.currentImage !== undefined) {
      const data = [
                    {key: "File Name: ", data:"11.jpg"},
                    {key: "File Size:", data:"3.2 MB"},
                    {key: "File Type:", data:"JPEG"},
                    {key: "File Type Extension:", data:"jpg"},
                    {key: "Mime Type:", data:"image/jpeg"},
                    {key: "Jfif Version:", data:"1.01"},
                    {key: "Make:", data:"Canon"},
                    {key: "Model:", data:"Canon EOS 6D"},
                    {key: "X Resolution:", data:"300"},
                    {key: "Y Resolution:", data:"300"},
                    {key: "Resolution Unit:", data:"inches"},
                    {key: "Software:", data:"Adobe Photoshop Lightroom 6.9"},
                    {key: "Modify Date:", data:"2017:03:22 22:20:03"}
      ]

      const popover = (
        <Popover id="popover-basic" title="Metadata" style={{zIndex: 3000}}>

        {data.map(e => {
          return(
            <Row>
              <Col>
                <p className="mb-0"><strong>{e.key}</strong></p>
              </Col>
              <Col>
                <p className="mb-0">{e.data}</p>
              </Col>
            </Row>
          )
        })}

        </Popover>
      );

      return (
        <OverlayTrigger trigger="click" placement="bottom" overlay={popover}>
          <Button variant="primary" >Visa metadata</Button>
        </OverlayTrigger>);
    }

  }

  displayPictureInformation() {
    if (this.state.images.length > 0 & this.state.currentImage !== undefined) {
      const data = [
                    {key: "Filnamn: ", data:`${this.state.images[this.state.currentImage].src.replace("/getPicture?id=", "")}`},
                    {key: "Datum:", data:`${this.state.images[this.state.currentImage].date}`},
                    {key: "Fotograf:", data:"Ahmed"},
                    {key: "Upplösning:", data:"6181x4121"},
                    {key: "Bildtext:", data:`${this.state.images[this.state.currentImage].caption}`},
                    {key: "Bildtaggar:", data:`${this.state.images[this.state.currentImage].tags.map(tag => {return (" " + tag.value)})}`},
                    {key: "Antal användningar:", data:"17 st"}
      ]

      const popover = (
        <Popover id="popover-basic" title="Bildinformation" style={{zIndex: 3000}}>

        {data.map(e => {
          return(
            <Row>
              <Col xs={6}>
                <p className="mb-0"><strong>{e.key}</strong></p>
              </Col>
              <Col>
                <p className="mb-0">{e.data}</p>
              </Col>
            </Row>
          )
        })}

        </Popover>
      );

      return (
        <OverlayTrigger trigger="click" placement="bottom" overlay={popover}>
          <Button variant="primary">Visa bildinformation</Button>
        </OverlayTrigger>);
    }
  }

  handleBuyImage() {
    let newString = this.state.images[this.state.currentImage].src.replace("/getPicture", "/downloadPicture")
    console.log(newString)
    window.open(`http://localhost:3100${newString}`, '_blank');
  }

  displayBuyPicture() {
    return (
        <Button onClick={this.handleBuyImage} variant="primary">Hämta bild</Button>
    )
  }


  render() {
    return (
      <Container>
        <Row>
          <Col>
          {this.showNumberofPictures()}
          </Col>
        </Row>
        <Row>

          <Col xs lg="2" className="text-center">
            Filter
              <Col className="form-control move-left text-left">
                {this.renderFilters()}
              </Col>
          </Col>

          <Col className="text-center">
            Galleriet
              <Col>
                <div className="form-control" style={{
                        display: "block",
                        minHeight: "1px",
                        width: "100%",
                        overflow: "auto"}}>
                  <Gallery images={this.state.images}
                   enableImageSelection={false}
                   currentImageWillChange={this.onCurrentImageChange}
                   customControls={[
                     <div>
                     <Row>
                     <Col>
                        {this.displayBuyPicture()}
                      </Col>
                     <Col>
                        {this.displayPictureInformation()}
                      </Col>
                      <Col>
                        {this.displayMetadata()}
                      </Col>
                      </Row>
                     </div>
                   ]}/>
                </div>
              </Col>
          </Col>

        </Row>
      </Container>
      );
  }
}

export default PictureGallery;
