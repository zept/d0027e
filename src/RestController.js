import React from 'react';

class RestController extends React.Component {

	constructor(props) {
		super(props);
		this.state = {images: [],
      };
	}

	componentDidMount() {
		fetch('/getPictures')
			.then(response => {
				return response.json();
			}).then(result => {
				this.setState({
					images:result
				});
			});
	}

  render() {
    return (this.state.images);
  }
}

export default RestController;
