import React from 'react';
import Button from 'react-bootstrap/Button';
import axios from 'axios'
import Modal from 'react-bootstrap/Modal';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import 'react-day-picker/lib/style.css';
import Tags from './Tags';
import InputGroup from 'react-bootstrap/InputGroup';
import FormControl from 'react-bootstrap/FormControl';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';

class Upload extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      caption: '',
      date: '',
      tags: [],
      image: null,
      lgShow: false
    };
  }

    handleChange = (e) => {
      this.setState({
        [e.target.id]: e.target.value
      })
    };

    handleImageChange = (e) => {
      this.setState({
        image: e.target.files[0]
      })
    };

    handleChangeDate(day) {
      this.setState({date: day});
      {/* Använd day.toISOString() för att skicka in till backend */}
    }

    handleSearchTags(tags) {
      this.setState({tags: tags});
    }

    handleSubmit = (e) => {
      e.preventDefault();
      console.log(this.state);
      let form_data = new FormData();
      form_data.append('image', this.state.image, this.state.image.name);
      form_data.append('caption', this.state.caption);
      form_data.append('date', this.state.date);
      this.state.tags.map(tag => {form_data.append('tags', tag.text)});
      let url = '/postPicture';
      axios.post(url, form_data, {
        headers: {
          'content-type': 'multipart/form-data'
        }
      })
          .then(res => {
            console.log(res.data);
          })
          .catch(err => console.log(err))
      this.handleCloseModal();
    };

    handleCloseModal() {
      this.setState({ lgShow: false })
    }


    render() {
		let lgClose = () => this.setState({ lgShow: false });

      return (
        <div>
        <Button variant="link" size="lg" onClick={() => this.setState({ lgShow: true })}>
                Ladda upp bild
        </Button>

        <Modal
					size="lg"
					show={this.state.lgShow}
					onHide={lgClose}
					aria-labelledby="example-modal-sizes-title-lg"
				>
					<Modal.Header closeButton>
						<Modal.Title id="example-modal-sizes-title-lg">
							Ladda upp bild
						</Modal.Title>
					</Modal.Header>
					<Modal.Body>

          <Row className="justify-content-md-center">
            <Col md="auto" align="center">
            <label htmlFor="FromDate"><strong>Datum</strong></label>
              <InputGroup className="mb-3">
                  <DayPickerInput
                    id="date"
                    inputProps={{className:'form-control'}}
                    onDayChange={day => this.handleChangeDate(day.toISOString())} />
              </InputGroup>
              </Col>
          </Row>

          <Row className="justify-content-md-center">
            <Col md="auto" align="center">
            <label htmlFor="caption"><strong>Bildtext</strong></label>
              <InputGroup className="mb-3">
                <FormControl
                  placeholder="Ange bildtext"
                  id="caption"
                  aria-label="Default"
                  aria-describedby="inputGroup-sizing-default"
                  value={this.state.caption}
                  onChange={this.handleChange}
                />
              </InputGroup>
              </Col>
          </Row>

          <Row className="justify-content-md-center">
  					<Col md="auto" align="center">
  						<label className="label-primary"><strong>Söktaggar: </strong></label>
  							<InputGroup className="mb-3">
  									<Tags
  										onTagChange={tags => this.handleSearchTags(tags)} />
  									<br/>
  							</InputGroup>
  						</Col>
  				 </Row>

           <Row className="justify-content-md-center">
            <Col md="auto" align="center">
              <label className="label-primary"><strong>Välj bild: </strong></label>
                <InputGroup className="mb-3">
                  <form onSubmit={this.handleSubmit}>
                      <input class="btn btn-link"
                             type="file"
                             id="image"
                             accept="image/png, image/jpeg"  onChange={this.handleImageChange} required/>
                             <br/>
                             <br/>
                    <input block className="btn btn-primary btn-block" type="submit" value="Slutför"/>
                  </form>
                </InputGroup>
              </Col>
           </Row>

        </Modal.Body>
      </Modal>
      </div>
      );
    }
  }

export default Upload;
