import React from 'react';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Alert from 'react-bootstrap/Alert';
import bigLogo from './img/BB.png';

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      loginInfo: undefined,
      firstLoad: 1,
    };
    this.handleEmail = this.handleEmail.bind(this);
    this.handlePassword = this.handlePassword.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(event) {
    event.preventDefault();
    fetch(`/getCredentials?user=${this.state.email}&password=${this.state.password}`)
      .then(response => {
        return response.json();
      }).then(result => {
        this.setState({
          loginInfo:result,
          firstLoad: 0,
        });
        this.props.onSuccessfulLogin(this.state.loginInfo);
      });
  }

  handleEmail(event) {
    this.setState({email: event.target.value});
  }

  handlePassword(event) {
    this.setState({password: event.target.value});
  }

  displayLoginFailed() {
    if (this.state.firstLoad != 1) {
      return (
        <Alert key='0' variant='danger'>
        Inloggningen misslyckades, kontrollera dina uppgifter.
        </Alert>);
    } else {
      return null;
    }

  }

  render() {
    return (
      <div>
      <div className="pt-5">
        <img src={bigLogo} alt="Logo" />
      </div>
      <h6> Inloggning bildbank</h6>
      <br/>
      {this.displayLoginFailed()}
      <br/>
      <div class="row justify-content-md-center">
        <div class="col-xs-6 my-auto">
          <Form>
            <Form.Group controlId="formBasicEmail">
              <Form.Label><strong>E-postadress</strong></Form.Label>
              <Form.Control value={this.state.email} placeholder="Ange e-postadress" type="email" onChange={this.handleEmail} />
              <Form.Text className="text-muted">
              </Form.Text>
            </Form.Group>

            <Form.Group controlId="formBasicPassword">
              <Form.Label><strong>Lösenord</strong></Form.Label>
              <Form.Control value={this.state.password} type="password" placeholder="Ange lösenord" onChange={this.handlePassword} />
            </Form.Group>

            <Button variant="primary" type="submit" onClick={this.handleSubmit}>
              Logga in
            </Button>
          </Form>
        </div>
      </div>
      </div>
      );
  }
}

export default Login;
