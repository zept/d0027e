import React from 'react';
import ReactDOM from 'react-dom';
import SearchBar from './SearchBar';
import './css/bootstrap.css';
import Navbar from 'react-bootstrap/Navbar';
import Button from 'react-bootstrap/Button';
import Nav from 'react-bootstrap/Nav';
import { ReactComponent as Logo } from './img/bothniabladet_logo.svg';
import logo from './img/BB_logo.png';
import Upload from './Upload.js';
import WelcomeMessage from './WelcomeMessage.js'
import Login from './Login.js'
import PictureGallery from './PictureGallery.js'
import './css/style.css'

class MainView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      images: [],
      firstLoad: 1,
      loginKey: null,
      role: null,
    };
    this.handleLogin = this.handleLogin.bind(this);
  }

  handleSearch(searchParams) {
    // Läs in alla taggar till en array

      fetch(`/getPictures?fromDate=${searchParams.fromDate}&toDate=${searchParams.toDate}${searchParams.tags.map(tag => "&tags=" + tag.text).join('')}`)
        .then(response => {
          return response.json();
        }).then(result => {
          this.setState({
            images:result,
            firstLoad: 0
          });
        });
  }

  handleLogin(login) {
    if (login) {
      this.setState({loginKey: login.loginKey,
                    role: login.role})
    }
  }

      displaySeachButton() {
        if (this.state.role == "Photographer" || this.state.role == "User") {
          return(
            <SearchBar
          onClick={(e) => this.handleSearch(e)} />
        );
        }
      }

      displayUploadButton() {
        if (this.state.role == "Photographer") {
          return(
            <Upload />
        );
        }
      }

      displayLogOutButton() {
        if (this.state.role == "Photographer" || this.state.role == "User") {
          return (<Button variant="link" size="lg" onClick={() => this.setState({ role: null, loginKey: null, firstLoad: 1  })}>
                  Logga ut
          </Button>);
        }
      }

      displayMainArea() {
        if (this.state.loginKey === null || this.state.loginKey === "") {
          return (
            <Login onSuccessfulLogin={this.handleLogin}/>
          )
        }
        if (this.state.firstLoad === 1) {
          return (
            <WelcomeMessage />
          );
        } else {
          return (
            <PictureGallery
            imageGallery={this.state.images}
            key={this.state.images}/>
            );
        }
      }

      getName(){
        return this.state.email;
      }

  render() {

    return (
      <div align="center">
        <Navbar bg="light" expand="lg">
          <Navbar.Brand><img src={logo} alt="Logo" /></Navbar.Brand>
            <Navbar.Collapse className="justify-content-right">
            <Nav className="mr-auto">
              {this.displaySeachButton()}
              {this.displayUploadButton()}
            </Nav>
            <Nav className="userStatus">
            <li><a href="#">{this.displayLogOutButton()}</a></li>
            </Nav>
            </Navbar.Collapse>
        </Navbar>
        <nav className="navbar2">
            <div className="containerB">
              <ul>
                  <li><a href="#">Hem</a></li>
                  <li><a href="#">Om Oss</a></li>
                  <li><a href="#">Kontakt</a></li>
                  <li><a href="#">Tipsa oss</a></li>
              </ul>
          </div>
        </nav>

        <div class="container"  style={{padding:"20px"}}>
          {this.displayMainArea()}
        </div>
      </div>
    );
  }
}

export default MainView;
