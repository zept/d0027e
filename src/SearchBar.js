import React from 'react';
import Button from 'react-bootstrap/Button';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import 'react-day-picker/lib/style.css';
import Tags from './Tags';
import InputGroup from 'react-bootstrap/InputGroup';
import FormControl from 'react-bootstrap/FormControl';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Modal from 'react-bootstrap/Modal';

class SearchBar extends React.Component {
	constructor(props) {
		super(props);
    {/* Alla sökparametrar här */}
		this.state = {fromDate: "1970-01-01T10:00:00.000Z",
									toDate: "2050-12-31T10:00:00.000Z",
									tags: [],
									lgShow: false};

	}

	handleChangeFromDate(day) {
		this.setState({fromDate: day});
		{/* Använd day.toISOString() för att skicka in till backend */}
	}

	handleChangeToDate(day) {
		this.setState({toDate: day});
		{/* Använd day.toISOString() för att skicka in till backend */}
	}

	handleSearchTags(tags) {
		this.setState({tags: tags});
	}

	handleCloseModal() {
		this.setState({ lgShow: false })
	}

  render() {
		let lgClose = () => this.setState({ lgShow: false });

    return (
			<Container>
				<Button variant="link" size="lg" onClick={() => this.setState({ lgShow: true })}>
				          Sök i bildbanken
				</Button>

				<Modal
					size="lg"
					show={this.state.lgShow}
					onHide={lgClose}
					aria-labelledby="example-modal-sizes-title-lg"
				>
					<Modal.Header closeButton>
						<Modal.Title id="example-modal-sizes-title-lg">
							Sök i bildbanken
						</Modal.Title>
					</Modal.Header>
					<Modal.Body>

					<Row className="justify-content-md-center">
						<Col md="auto" align="center">
						<label htmlFor="FromDate"><strong>Från Datum: </strong></label>
							<InputGroup className="mb-3">
									<DayPickerInput id="FromDate"
									 	inputProps={{className:'form-control'}}
										onDayChange={day => this.handleChangeFromDate(day.toISOString())} />
							</InputGroup>
							</Col>
					</Row>

					<Row className="justify-content-md-center">
						<Col md="auto" align="center">
							<label className="label-primary"><strong>Till Datum: </strong></label>
							<InputGroup className="mb-3">
									<DayPickerInput
										inputProps={{className:'form-control'}}
										onDayChange={day => this.handleChangeToDate(day.toISOString())} />
							</InputGroup>
						</Col>
					</Row>

				<Row className="justify-content-md-center">
					<Col md="auto" align="center">
						<label className="label-primary"><strong>Söktaggar: </strong></label>
							<InputGroup className="mb-3">
									<Tags
										onTagChange={tags => this.handleSearchTags(tags)} />
									<br/>
									<br/>
							</InputGroup>
							<Button
								block
								as="input"
								type="button"
								value="Sök"
								onClick={() => { this.props.onClick(this.state); this.handleCloseModal()}}
								/>
						</Col>
				 </Row>



					</Modal.Body>
				</Modal>

			</Container>
    );
  }
}

export default SearchBar;
